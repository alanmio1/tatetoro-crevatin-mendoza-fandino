package TaTeToroJuego;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Interfaz {

	private JFrame frame;
	
	private int turno = 1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
	}
	
	public void cambiarTurno() {
		
		if (turno == 1) {
			turno = 2;
		}
		else {
			turno = 1;
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		final JButton button0 = new JButton("");
		button0.setForeground(Color.BLUE);
		button0.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button0.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				if(turno == 1) {
					button0.setText("X");
				}
				else {
					button0.setText("O");
				}
				button0.setEnabled(false);
			}
		});
		button0.setBounds(49, 21, 50, 50);
		frame.getContentPane().add(button0);
		
		final JButton button1 = new JButton("");
		button1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button1.setText("X");
				}
				else {
					button1.setText("O");
				}
			}
		});
		button1.setForeground(Color.BLUE);
		button1.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button1.setBounds(121, 21, 50, 50);
		frame.getContentPane().add(button1);
		
		final JButton button2 = new JButton("");
		button2.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button2.setText("X");
				}
				else {
					button2.setText("O");
				}
			}
		});
		button2.setForeground(Color.BLUE);
		button2.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button2.setBounds(195, 21, 50, 50);
		frame.getContentPane().add(button2);
		
		final JButton button3 = new JButton("");
		button3.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button3.setText("X");
				}
				else {
					button3.setText("O");
				}
			}
		});
		button3.setForeground(Color.BLUE);
		button3.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button3.setBounds(49, 90, 50, 50);
		frame.getContentPane().add(button3);
		
		final JButton button4 = new JButton("");
		button4.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button4.setText("X");
				}
				else {
					button4.setText("O");
				}
			}
		});
		button4.setForeground(Color.BLUE);
		button4.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button4.setBounds(121, 90, 50, 50);
		frame.getContentPane().add(button4);
		
		final JButton button5 = new JButton("");
		button5.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button5.setText("X");
				}
				else {
					button5.setText("O");
				}
			}
		});
		button5.setForeground(Color.BLUE);
		button5.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button5.setBounds(195, 90, 50, 50);
		frame.getContentPane().add(button5);
		
		final JButton button6 = new JButton("");
		button6.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button6.setText("X");
				}
				else {
					button6.setText("O");
				}
			}
		});
		button6.setForeground(Color.BLUE);
		button6.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button6.setBounds(49, 161, 50, 50);
		frame.getContentPane().add(button6);
		
		final JButton button7 = new JButton("");
		button7.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button7.setText("X");
				}
				else {
					button7.setText("O");
				}
			}
		});
		button7.setForeground(Color.BLUE);
		button7.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button7.setBounds(121, 161, 50, 50);
		frame.getContentPane().add(button7);
		
		final JButton button8 = new JButton("");
		button8.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(turno == 1) {
					button8.setText("X");
				}
				else {
					button8.setText("O");
				}
			}
		});
		button8.setForeground(Color.BLUE);
		button8.setFont(new Font("Trebuchet MS", Font.PLAIN, 23));
		button8.setBounds(195, 161, 50, 50);
		frame.getContentPane().add(button8);
		
		
		JButton btnReiniciar = new JButton("Reiniciar");
		btnReiniciar.setBounds(271, 212, 89, 23);
		frame.getContentPane().add(btnReiniciar);
	}
}
