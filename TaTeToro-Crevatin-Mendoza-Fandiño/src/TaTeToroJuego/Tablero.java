package TaTeToroJuego;

public class Tablero {
	
	int botones [] = new int [8];
	
	public void marcarBoton (int botonAMarcar, int  valor) {
		
		for (int i = 0; i < botones.length; i++) {
			if (botones[i] == i) {
				botones [i] = valor;
			}
		}
	}
	
	public boolean gano() 
	{
		boolean bandera = false;
		
		if ( botones[0] !=0 && botones[0] == botones[1] && botones[0] == botones[2])
		{
			bandera = true;
		}
		if ( botones[3] !=0 && botones[3] == botones[4] && botones[3] == botones[5])
		{
			bandera = true;
		}
		if ( botones[6] !=0 && botones[6] == botones[7] && botones[6] == botones[8])
		{
			bandera = true;
		}
		if ( botones[0] !=0 && botones[0] == botones[3] && botones[0] == botones[6])
		{
			bandera = true;
		}
		if ( botones[1] !=0 && botones[1] == botones[4] && botones[1] == botones[7])
		{
			bandera = true;
		}
		if ( botones[2] !=0 && botones[2] == botones[5] && botones[2] == botones[8])
		{
			bandera = true;
		}
		if ( botones[0] !=0 && botones[0] == botones[4] && botones[0] == botones[8])
		{
			bandera = true;
		}
		if ( botones[2] !=0 && botones[2] == botones[4] && botones[2] == botones[6])
		{
			bandera = true;
		}
		if ( botones[1] !=0 && botones[1] == botones[5] && botones[1] == botones[6])
		{
			bandera = true;
		}
		if ( botones[0] !=0 && botones[0] == botones[5] && botones[0] == botones[7])
		{
			bandera = true;
		}
		if ( botones[1] !=0 && botones[1] == botones[3] && botones[1] == botones[8])
		{
			bandera = true;
		}
		if ( botones[2] !=0 && botones[2] == botones[3] && botones[2] == botones[7])
		{
			bandera = true;
		}
		return bandera;
		}
	
	public void reiniciarTablero () {
		for (int i = 0; i < botones.length; i++) {
			botones[i] = 0;
		}
	}
	
	}	
